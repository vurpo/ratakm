FROM python:3.12-slim-bullseye

RUN apt-get update
RUN apt-get install -y proj-bin

COPY requirements.txt /opt/ratakm/requirements.txt

RUN pip3 install -r /opt/ratakm/requirements.txt

COPY . /opt/ratakm/

WORKDIR /opt/ratakm

CMD python3 main.py