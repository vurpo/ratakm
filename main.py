# -*- coding: utf-8 -*-

import asyncio
from aiohttp import ClientSession, web
import requests
import pprint
import json
from jinja2 import Environment, FileSystemLoader
from datetime import datetime
from pyproj import Transformer

pp = pprint.PrettyPrinter()

transformer = Transformer.from_crs(4326, 3067)
reverse_transformer = Transformer.from_crs(3067, 4326)

# Retreive and mangle data from Digitraffic

radat_ = requests.get("https://rata.digitraffic.fi/infra-api/0.2/radat.json?srsName=crs:84").json()
radat = dict([
    (rata[1][0]['ratanumero'], {"rata":rata[1][0], "rliikennepaikat":[]})
    for rata in radat_.items()
])

def transmuteRatakmsijainti(rlp):
    sijainnit = {}

    if rlp['virallinenRatakmsijainti'] is not None:
        virallinenRata = rlp['virallinenRatakmsijainti']['ratanumero']
        sijainnit[rlp['virallinenRatakmsijainti']['ratanumero']] = {
            "ratakm": rlp['virallinenRatakmsijainti']['ratakm'],
            "etaisyys": rlp['virallinenRatakmsijainti']['etaisyys']
        }
    else:
        virallinenRata = None

    for sijainti in rlp['muutRatakmsijainnit']:
        sijainnit[sijainti['ratanumero']] = {
            "ratakm": sijainti['ratakm'],
            "etaisyys": sijainti['etaisyys']
        }
    
    rlp['sijainnit'] = sijainnit
    rlp['virallinenRata'] = virallinenRata

    del rlp['virallinenRatakmsijainti']
    del rlp['muutRatakmsijainnit']

    return rlp

rliikennepaikat = requests.get("https://rata.digitraffic.fi/infra-api/0.2/rautatieliikennepaikat.json?srsName=crs:84").json()

for rlp in rliikennepaikat.values():
    rlp = transmuteRatakmsijainti(rlp[0])

    for ratanumero, sijainti in rlp['sijainnit'].items():
        radat[ratanumero]['rliikennepaikat'].append(rlp)

rliikennepaikat = dict([
    (rlp[0]['lyhenne'], rlp[0])
    for rlp in rliikennepaikat.values()
])

rlptyypit = requests.get("https://rata.digitraffic.fi/infra-api/0.2/rautatieliikennepaikkatyypit.json").json()
tyyppiotsikot = {
    "linjavaihde": "linjavaihteet",
    "seisake": "seisakkeet",
    "liikennepaikka": "liikennepaikat",
    "muu": "muut rautatieliikennepaikat"
}

for ratanumero, rata in radat.items():
    rata['rliikennepaikat'].sort(key=lambda rlp: (rlp['sijainnit'][ratanumero]['ratakm']*10000)+(rlp['sijainnit'][ratanumero]['etaisyys']))

async def fetch(url, session):
    async with session.get(url) as response:
        return await response.read()

def kmOnRoad(ranges, km: int):
    km = int(km)
    if len(ranges) == 0:
        return False
    if type(ranges[0]) == int and len(ranges) == 1:
        return km == ranges[0]
    elif type(ranges[0]) == int and len(ranges) > 1:
        return km in range(ranges[0],ranges[1]+1)
    elif type(ranges[0]) == list:
        return True in [kmOnRoad(r, km) for r in ranges]
    return False

async def kilometri(km):
    url = "https://rata.digitraffic.fi/infra-api/0.2/radat/{}/{}.json?srsName=crs:84"
    tasks = {}
    ratanrot = [ratanro for ratanro in radat if kmOnRoad(radat[ratanro]['rata']['ratakilometrit'], km.split('+')[0])]

    async with ClientSession() as session:
        for ratanumero in ratanrot:
            task = asyncio.ensure_future(session.get(url.format(ratanumero, km)))
            tasks[ratanumero] = task

        results = await asyncio.gather(*tasks.values())
        responses = dict(zip(tasks.keys(), results))
        print(responses)
        responses = {ratanumero:(await km.json()) for (ratanumero, km) in responses.items() if km.status == 200}

        # you now have all response bodies in this variable
        return responses

def canonicalize_rlp_lyh(lyh):
    return "{}{}".format(lyh[0].upper(), lyh[1:].lower())

# Web server

routes = web.RouteTableDef()

@routes.get('/.json')
async def root_json(request):
    return web.Response(content_type='application/json', text=json.dumps(radat))

@routes.get('/kilometri/{sijainti}.json')
async def ratakm_json(request):
    return web.Response(content_type='application/json', text=json.dumps(await kilometri(request.match_info['sijainti'])))

@routes.get('/rata/{ratanumero}.json')
async def rata_json(request):
    if request.match_info['ratanumero'] in radat:
        return web.Response(content_type='application/json', text=json.dumps(radat[request.match_info['ratanumero']]))
    else:
        return web.Response(content_type='application/json', text=json.dumps({"error":"Not found"}, status=404))

@routes.get('/liikennepaikka/{lyhenne}.json')
async def liikennepaikka_json(request):
    if request.match_info['lyhenne'] in rliikennepaikat:
        return web.Response(content_type='application/json', text=json.dumps(rliikennepaikat[request.match_info['lyhenne']]))
    else:
        return web.Response(content_type='application/json', text=json.dumps({"error":"Not found"}, status=404))

@routes.get('/liikennepaikat/{tyyppi}.json')
async def liikennepaikat_json(request):
    if request.match_info['tyyppi'] in rlptyypit:
        return web.Response(content_type='application/json', text=json.dumps(dict(filter(lambda rlp: rlp[1]['tyyppi'] == request.match_info['tyyppi'], rliikennepaikat.items()))))
    else:
        return web.Response(content_type='application/json', text=json.dumps({"error":"Not found"}, status=404))

def rliikennepaikat_template_helper(rlpt):
    if len(rlpt) == 0:
        return "-"
    elif len(rlpt) == 1:
        return rlpt[0]['nimi']
    else:
        return "{} - {}".format(rlpt[0]['nimi'], rlpt[-1]['nimi'])

def ratakm_template_helper(ratakm):
    if len(ratakm) == 0:
        return "-"
    elif isinstance(ratakm[0], list):
        return ", ".join(["{}-{}".format(v[0],v[1]) for v in ratakm])
    else:
        return ratakm[0]

def sijainti_sorting_key(sijainti):
    return "{}".format(sijainti['ratakm']*10000+sijainti['etaisyys'])

def today():
    return datetime.now().strftime("%d.%m.%Y")

def karttapaikka_linkki(coord, name):
    coord = transformer.transform(coord[1], coord[0])
    return "https://asiointi.maanmittauslaitos.fi/karttapaikka/?share=customMarker&n={}&e={}&title={}&desc=&zoom=10&layers=%5B%7B%22id%22%3A2%2C%22opacity%22%3A100%7D%5D".format(coord[1], coord[0], name)

def debug(text):
  print(text)
  return ''

file_loader = FileSystemLoader('templates')
env = Environment(loader=file_loader)
env.filters['rliikennepaikat'] = rliikennepaikat_template_helper
env.filters['ratakm'] = ratakm_template_helper
env.filters['sijainti_sorting_key'] = sijainti_sorting_key
env.filters['debug']=debug
env.globals.update(list=list, len=len, today=today, karttapaikka_linkki=karttapaikka_linkki)
root_template = env.get_template("root.html")
ratakmplus_template = env.get_template("ratakmplus.html")
ratakm_template = env.get_template("ratakm.html")
rata_template = env.get_template("rata.html")
liikennepaikka_template = env.get_template("liikennepaikka.html")
liikennepaikat_template = env.get_template("liikennepaikat.html")

@routes.get('/')
async def root_html(request):
    return web.Response(
        content_type='text/html',
        text=root_template.render(
            radat=sorted(sorted(radat.items(), key=lambda x: x[0]), key=lambda x: len(x[0]))
        )
    )

@routes.get('/kilometri/{sijainti}+{etaisyys}')
async def ratakmplus_html(request):
    return web.Response(
        content_type='text/html',
        text=ratakmplus_template.render(
            sijainti="{}+{}".format(request.match_info['sijainti'], request.match_info['etaisyys']),
            kilometri=await kilometri("{}+{}".format(request.match_info['sijainti'], request.match_info['etaisyys'])),
            radat=radat
        )
    )

@routes.get('/kilometri/{sijainti}')
async def ratakm_html(request):
    return web.Response(
        content_type='text/html',
        text=ratakm_template.render(
            sijainti=request.match_info['sijainti'],
            kilometri=await kilometri(request.match_info['sijainti']),
            radat=radat
        )
    )

@routes.get('/rata/{ratanumero}')
async def rata_html(request):
    if request.match_info['ratanumero'] in radat:
        return web.Response(
            content_type='text/html',
            text=rata_template.render(
                ratanumero=request.match_info['ratanumero'],
                rata=radat[request.match_info['ratanumero']]
            )
        )
    else:
        return web.Response(text="Not found", status=404)

@routes.get('/liikennepaikka/{lyhenne}')
async def liikennepaikka_html(request):
    lyhenne = request.match_info['lyhenne']

    if lyhenne not in rliikennepaikat and canonicalize_rlp_lyh(lyhenne) in rliikennepaikat:
        return web.HTTPFound('/liikennepaikka/{}'.format(canonicalize_rlp_lyh(lyhenne)))
    if lyhenne in rliikennepaikat:
        return web.Response(
            content_type='text/html',
            text=liikennepaikka_template.render(
                lyhenne=lyhenne,
                liikennepaikka=rliikennepaikat[lyhenne],
                radat=radat
            )
        )
    else:
        return web.Response(text="Not found", status=404)

@routes.get('/liikennepaikat/{tyyppi}')
async def liikennepaikat_tyyppi_html(request):
    if request.match_info['tyyppi'] in rlptyypit:
        return web.Response(
            content_type='text/html',
            text=liikennepaikat_template.render(
                otsikko=tyyppiotsikot.get(request.match_info['tyyppi'], request.match_info['tyyppi']),
                rliikennepaikat=list(filter(lambda rlp: rlp['tyyppi'] == request.match_info['tyyppi'], rliikennepaikat.values())),
                radat=radat
            )
        )
    else:
        return web.Response(text="Not found", status=404)

@routes.get('/ratakmhaku')
async def ratakmhaku(request):
    try:
        return web.HTTPFound('/kilometri/{}'.format(request.rel_url.query['haku']))
    except KeyError:
        return web.Response(text="Missing key 'haku'", status=400)

@routes.get('/rlphaku')
async def rlphaku(request):
    try:
        lyhenne = canonicalize_rlp_lyh(request.rel_url.query['haku'])
        return web.HTTPFound('/liikennepaikka/{}'.format(lyhenne))
    except KeyError:
        return web.Response(text="Missing key 'haku'", status=400)

app = web.Application()
app.add_routes(routes)
app.add_routes([web.static('/static', "./static")])
web.run_app(app)
